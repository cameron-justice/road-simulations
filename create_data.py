import random
import math

random.seed(random.randint(20, 1000))

shape = (4, random.randint(4, 30))
routes = [[0, 0, 0, 0] for i in range(shape[1])]

to_connect = [i for i in range(shape[1])]

for (ix, route) in enumerate(routes):
    print(ix, route)
    start = random.choice(to_connect)
    to_connect.remove(start)
    end = random.choice([i for i in range(shape[1]) if i != start])
    weight = random.choice([round(random.random() * 5, 2) for i in range(10)])
    speed = random.choice([random.randint(1, 15) * 5 for i in range(10)])

    routes[ix][0] = start
    routes[ix][1] = end
    routes[ix][2] = weight
    routes[ix][3] = speed

with open(f'data/{shape[0]}-{shape[1]}.rnd', 'w+') as f:
    for route in routes:
        f.write(route.__str__().replace('[', '').replace(']', '').replace(',', ''))
        f.write('\n')
import random
import numpy as np
import heapq


def astar(root):
    # Generates and initialized the frontier and closed list
    frontier = PriorityQueue()
    frontier.push(root)
    closed_list = Set()

    # Continually visits the frontier
    while not frontier.isEmpty():
        curNode = frontier.pop()

        # Goal checking
        found = True
        # Rest of the fucking goal checking

        if found:
            break

        # Node is not goal, goal gets added to list
        closed_list.add(curNode.state)

        # Generates the child states
        childStates = adjacency.lookup(curNode)

        # g(n) = f(parent) - h(parent) + 1(path cost)
        path_cost = 1
        g = curNode.val - heuristic(curNode.state) + 1 * path_cost

        for child in childStates:
            if child != None and not closed_list.is_member(child):
                f = g + heuristic(child)  # f(n) = g(n) + h(n)
                newNode = Node(f, curNode, child)
                frontier.push(newNode)

        v = closed_list.length()  # Total visited/expanded
        n = v + frontier.length()  # Max nodes stored
        d = curNode.val - heuristic(curNode.state)  # Depth
        b = pow(n, 1 / d) if d > 0 else 0  # -Approximate- branching factor

        print(curNode)  # Print the optimal path


def heuristic(aState):
    # Distance from goal
    return 0


# Class for frontier
class PriorityQueue:
    def __init__(self):
        self.Queue = []

    def push(self, aNode):
        heapq.heappush(self.Queue, (aNode.val, -aNode.id, aNode))

    def pop(self):
        return heapq.heappop(self.Queue)[2]  # Pops off and returns ID

    def length(self):
        return len(self.Queue)

    def is_empty(self):
        return self.length() == 0


# Class for closed list
class Set:
    def __init__(self):
        self.Set = set()

    def add(self, item):
        if item is not None:
            self.Set.add(item.__hash__())

    def length(self):
        return len(self.Set)

    def is_member(self, query):
        return query.__hash__() in self.Set


class Node:
    def __init__(self, val, parent, state):
        global nodeid
        self.id = nodeid
        nodeid += 1
        self.val = val
        self.parent = parent
        self.state = state

    def __str__(self):
        return 'Node: id=%d val=%d' % (self.id, self.val)

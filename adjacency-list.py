import numpy as np

large_width = 400
np.set_printoptions(linewidth=large_width)

class AdjacencyList:
    def build(self, fileName):
        with open(fileName, 'r') as inFile:
            row, col, edges = inFile.readline().split(' ')
            row = int(row)
            col = int(col)
            edges = int(edges)

            self.List = np.full((row, col), -1)


            for line in inFile:
                line = [float(x) for x in line.strip().split(' ')]
                self.List[line[0] - 1][line[1] - 1] = line[2] / line[3] * 3600 # path cost (in seconds)

            for row in range(len(self.List)):
                print(row, self.List[row])


lst = AdjacencyList()
lst.build("data.txt")

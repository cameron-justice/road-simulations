import itertools
import random
import simpy
import numpy as np
import  logging

RANDOM_SEED = 42
GAS_STATION_SIZE = 200  # Liters
THRESHOLD = 10  # Threshold for calling the tank truck (in %)
FUEL_TANK_SIZE = 50  # Liters
FUEL_TANK_LEVEL = [5, 25]  # Min/max levels of fuel tanks (in liters)
REFUELING_SPEED = 2  # Liters/second
TANK_TRUCK_TIME = 300  # Seconds it takes the tank truck to arrive
T_INTER = [30, 300]  # Create a car every [min, max] seconds
SIM_TIME = 1000  # Simulation time in seconds

# Positions are denoted as (X,Y) tuples
Position = (int, int)

# Grids are aligned from the top-left at (0, 0) and the bottom-right as (N,N)

logger = logging.getLogger('GRID_NAVIGATION')

class Actor:
    def __init__(self, name):

class GridManager:
    def __init__(self, env):
        self.env = env
        # setup grid
        self.grid = np.ndarray((5,5))
        self.actors = {}

    def move(self, actor: Actor, curr_pos: Position, next_pos: Position):
        if self.check_space(next_pos):
            self.open_space(curr_pos)
            self.close_space(next_pos)
            self.actors[actor.name] = next_pos
            return True
        else:
            return False

    def register(self, actor, pos):
        if self.check_space(pos):
            self.close_space(pos)
            self.actors[actor.name] = pos
            return True
        else:
            return False

    # Closed spaces are marked as '0'
    def close_space(self, pos):
        self.grid[pos[0],pos[1]] = 0

    # Open spaces are marked as '1'
    def open_space(self, pos):
        self.grid[pos[0],pos[1]] = 1

    # Is this space open?
    def check_space(self, pos):
        return self.grid[pos[0],pos[1]] == 1

    def report_repair(self, self1, pos):
        pass

MAX_REPAIR_PERC = 100

class Car(Actor):
    def __init__(self, env, name: str, gm: GridManager, start: Position, goal: Position):
        super().__init__(name)
        self.env = env # Simpy Environment
        self.name = name # Actor Name
        self.gm = gm # GridManager instance
        self.pos = start # Current Position
        self.goal = goal # Goal Position
        self.action = self.drive # Action the car should start on next cycle

        gm.register(self, self.pos) # Register actor with GridManager

        self.action()

    def drive(self):
        while self.pos != self.goal:
            try: # Car will wait until it can move to proceed
                next_pos = self.get_next_position()
                moved = yield self.gm.move(self, self.pos, next_pos)
                self.pos = next_pos if moved else self.pos
            except simpy.Interrupt:
                self.action = self.repair
                wait_event = yield self.gm.report_repair(self, self.pos)
                can_drive = yield wait_event
                if can_drive:
                    self.action()
                else:
                    yield self.gm.unregister(self, self.pos)
                    break

    def get_next_position(self):
        if abs(self.pos[0] - self.goal[0]):
            return [np.sign(self.goal - self.pos[0]), self.pos[1]]
        else:
            return [self.pos[0], np.sign(self.goal - self.pos[1])]

    def repair(self, percentage):
        self.repair_status = min(MAX_REPAIR_PERC, self.repair_status + percentage) # Cap at MAX_REPAIR_PERC


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print('Gas Station Refuelling')
    random.seed(RANDOM_SEED)
    env = simpy.Environment()
    gas_station = simpy.Resource(env, 2)
    fuel_pump = simpy.Container(env, GAS_STATION_SIZE, init=GAS_STATION_SIZE)
    env.process(gas_station_control(env, fuel_pump))
    env.process(car_generator(env, gas_station, fuel_pump))

    env.run(until=SIM_TIME)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
